import React from 'react'
import ReactDOM from 'react-dom'
import { BrowserRouter, Route, Switch } from 'react-router-dom'

import Home from './components/home'
import Search from './components/search'
import Error from './components/error'
import Navigation from './components/navigation'
import Footer from './components/footer'

import './index.scss'

const App = () => (
  <BrowserRouter>
    <div>
      <Navigation />
      <Switch>
        <Route path="/" component={Home} exact />
        <Route path="/search" component={Search} />
        <Route component={Error} />
      </Switch>
      <Footer />
    </div>
  </BrowserRouter>
)

ReactDOM.render(<App />, document.getElementById('app'))
