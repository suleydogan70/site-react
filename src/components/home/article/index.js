import React from 'react'
import { NavLink } from 'react-router-dom'

const Article = ({ event }) => (
  <article>
    <div>
      <img src={event.image} alt={event.title} />
    </div>
    <h3>{event.title}</h3>
    <p>{event.description}</p>
    <NavLink to={`/article/${event.id}`}>En savoir plus</NavLink>
  </article>
)

export default Article
