import React, { Component } from 'react'
import axios from 'axios'

import Article from './article'

class Home extends Component {
  constructor() {
    super()
    const initialState = {
      data: []
    }

    this.state = initialState
  }

  componentDidMount() {
    this.getData()
  }

  getData() {
    axios.get('https://opendata.paris.fr/api/records/1.0/search/?dataset=evenements-a-paris&facet=placename&facet=department&facet=region&facet=city&facet=date_start&facet=date_end&facet=pricing_info')
      .then((response) => {
        this.setState({
          data: this.formatEvents(response.data.records)
        })
      }, (err) => {
        console.log(err)
      })
  }

  /**
  * Format events
  * @param {Array} events
  * @return {Array} eventsFormatted
  */
  formatEvents(events) {
    return events.map(event => ({
      id: event.recordid,
      address: event.fields.address,
      city: event.fields.city,
      dateEnd: event.fields.date_end,
      dateStart: event.fields.date_start,
      description: event.fields.description,
      image: event.fields.image,
      title: event.fields.title
    }))
  }

  render() {
    const { data } = this.state
    return (
      <section>
        <h2>Actualités</h2>
        <div>
          {
            data.map(
              article => <Article key={article.id} event={article} />
            )
          }
        </div>
      </section>
    )
  }
}

export default Home
