import React from 'react'

const Footer = () => (
  <footer>
    © Site réalisé par Suleyman DOGAN
  </footer>
)

export default Footer
