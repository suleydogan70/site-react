import React, { Component } from 'react'
import axios from 'axios'

import Article from '../home/article'

class Search extends Component {
  constructor() {
    super()
    const initialState = {
      data: [],
      value: null
    }

    this.state = initialState
  }

  getData(value) {
    axios.get(`https://opendata.paris.fr/api/records/1.0/search/?dataset=evenements-a-paris&facet=placename&facet=department&facet=region&facet=city&facet=date_start&facet=date_end&facet=pricing_info&q=${value}`)
      .then((response) => {
        this.setState({
          data: this.formatEvents(response.data.records)
        })
      }, (err) => {
        console.log(err)
      })
  }

  /**
  * Format events
  * @param {Array} events
  * @return {Array} eventsFormatted
  */
  formatEvents(events) {
    return events.map(event => ({
      id: event.recordid,
      address: event.fields.address,
      city: event.fields.city,
      dateEnd: event.fields.date_end,
      dateStart: event.fields.date_start,
      description: event.fields.description,
      image: event.fields.image,
      title: event.fields.title
    }))
  }

  handleOnClick(e) {
    const { value } = this.state
    e.preventDefault()
    this.getData(value)
  }

  handleOnChange(e) {
    this.setState({
      value: e.currentTarget.value
    })
  }

  render() {
    const { data } = this.state
    return (
      <section>
        <h2>Recherche</h2>
        <form>
          <input type="text" onChange={this.handleOnChange.bind(this)} />
          <button type="submit" onClick={this.handleOnClick.bind(this)}>Chercher</button>
        </form>
        <div>
          {
            data.map(
              article => <Article key={article.id} event={article} />
            )
          }
        </div>
      </section>
    )
  }
}

export default Search
