import React, { Component } from 'react'
import { NavLink } from 'react-router-dom'

class Navigation extends Component {
  render() {
    return (
      <header>
        <nav>
          <h1>New App</h1>
          <ul>
            <li><NavLink to="/">Home</NavLink></li>
            <li><NavLink to="/search">Search</NavLink></li>
          </ul>
        </nav>
      </header>
    )
  }
}

export default Navigation
